IP=$(hostname -I | awk '{print $2}')

echo "START - install registry - "$IP

echo "[1]: install docker"
apt-get update -qq >/dev/null
apt-get install -qq -y git wget curl git >/dev/null
curl -fsSL https://get.docker.com | sh; >/dev/null
curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose


echo "[2]: install registry"
mkdir certs/
openssl req -x509 -newkey rsa:4096 -nodes -keyout certs/myregistry.key -out certs/myregistry.crt -days 365 -subj /CN=myregistry.my
apt install -qq -y apache2-utils
mkdir passwd && cd passwd/
##docker run --entrypoint htpasswd registry:2 -Bbn xavki password > passwd/htpasswd
htpasswd -Bbc registry.password ahmed ahmed
cd ..
mkdir data/
echo "
version: '3.5'
services:
  registry:
    restart: always
    image: registry:2
    container_name: registry
    ports:
      - 5000:5000
    environment:
      REGISTRY_HTTP_TLS_CERTIFICATE: /certs/myregistry.crt
      REGISTRY_HTTP_TLS_KEY: /certs/myregistry.key
      REGISTRY_AUTH: htpasswd
      REGISTRY_AUTH_HTPASSWD_PATH: /auth/registry.password
      REGISTRY_AUTH_HTPASSWD_REALM: Registry Realm
    volumes:
      - ./data:/var/lib/registry
      - ./certs:/certs
      - ./passwd:/auth
" > docker-compose-registry.yml

docker-compose -f docker-compose-registry.yml up -d

echo "END - install registry"


